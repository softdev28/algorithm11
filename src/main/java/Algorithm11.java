/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ACER
 */
public class Algorithm11 {

    public static void main(String[] args) {
        int[] A = {1, 7, 13, 14, 95, 63, 12, 82, 99, 82, 12, 63, 95, 14, 13, 7, 1};
        int nonDup = 0;
        for (int i = 0; i < A.length; i++) {
             nonDup ^= A[i];
        }
        System.out.println(nonDup);
    }
}
